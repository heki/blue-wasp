# Blue Wasp [![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

Blue Wasp is a simple program to send BLE advertisment packets to iOS, Android, and Windows devices from Linux.

Thank you <a href="https://github.com/RapierXbox">­@RapierXbox</a> and <a href="https://github.com/Willy-JL">­@WillyJL</a> for helping me understand the bluetooth packet structure and <a href="https://github.com/ECTO-1A">­@ECTO-1A</a> for discovering this bug/exploit.

> ### 🚫 Warning
> Only use this on devices you own or when you have permisson to use it.\
> This project is made for educational purposes ONLY and is NOT made to break laws and/or\
> for personal gain.

## Quick Usage

Advertise random devices on Android:

```
blue-wasp random android
```

Advertise specific device on iOS
```
blue-wasp specific apple-device airpods-pro
```

All possible platforms are:
- `apple-action` - Apple actions
- `apple-device` - Apple devices
- `apple` - Apple actions and devices
- `android` - Android devices
- `mobile` - Apple devices and messages and Android devices
- `windows` - Windows devices
- `apple-windows` Apple actions and devices and Windows devices
- `android-windows`Android and Windows devices
- `all` - Apple, Android, and Windows devices and Apple actions

### iOS 17 BLE crash exploit

If you want to use iOS 17 BLE crash exploit enable `-c` flag:

```
blue-wasp random apple -c
```

The exploit will will be used on every apple action advertise packet.

### Custom devices

You can add/remove/edit devices in `apple_actions.txt`, `apple_devices.txt`, `android_devices.txt`, and `windows_devices.txt`.

### Help

For more information use `help` command:

```
blue-wasp help
```

## Build

1. Clone the repo:

```
git clone https://gitlab.com/heki/blue-wasp
```

2. Enter to the repo's folder:

```
cd blue-wasp
```

3. Build the project:

```
cargo build --release
```

4. The binary can be found at `target/release/blue-wasp` inside the repo's folder.
