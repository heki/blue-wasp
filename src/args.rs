/*
Copyright (C) 2023 Heki <heki@heki.me>

This file is part of Blue Wasp.

Blue Wasp is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Blue Wasp is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Blue Wasp. If not, see <https://www.gnu.org/licenses/>.
*/

use clap::{Parser, Subcommand};

use crate::Platform;

#[derive(Parser)]
#[command(author, version, about)]
pub struct Arguments {
    #[command(subcommand)]
    pub command: Commands,
}

#[derive(Subcommand, PartialEq, Eq)]
pub enum Commands {
    /// Advertise random devices
    Random {
        /// Possible values are: apple, apple-action, apple-device, android, mobile, windows, apple-windows, android-windows, all
        platform: Platform,

        /// Use iOS 17 BLE crash exploit for Apple actions
        #[arg(short, long)]
        crash_exploit: bool,

        /// Set TX power, range is from -127 to +20
        #[arg(short, long, default_value_t = 20)]
        tx_power: i16,

        /// Set interval between sending advertising packets in milliseconds, minimum is 20
        #[arg(short, long, default_value_t = 20)]
        interval: u64,

        /// Set duration of sending advertising packets in milliseconds
        #[arg(short, long, default_value_t = 20)]
        duration: u64,
    },

    /// Advertise specific device
    Specific {
        /// Possible values are: apple-action, apple-device, android, windows
        platform: Platform,

        /// Name of the device you want to advertise
        device_name: String,

        /// Use iOS 17 BLE crash exploit for Apple actions
        #[arg(short, long)]
        crash_exploit: bool,

        /// Set TX power, range is from -127 to +20
        #[arg(short, long, default_value_t = 20)]
        tx_power: i16,

        /// Set interval between sending advertising packets in milliseconds, minimum is 20
        #[arg(short, long, default_value_t = 20)]
        interval: u64,
    },

    /// List devices for specific platform
    List {
        /// Possible values are: apple-action, apple-device, android, windows
        platform: Platform,
    },

    /// Listen for BLE advertising packets
    Listen,
}
