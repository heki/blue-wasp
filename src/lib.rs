/*
Copyright (C) 2023 Heki <heki@heki.me>

This file is part of Blue Wasp.

Blue Wasp is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Blue Wasp is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Blue Wasp. If not, see <https://www.gnu.org/licenses/>.
*/

pub mod args;
pub mod create;

#[derive(Clone, Copy, PartialEq, Eq)]
pub enum Platform {
    Apple,
    AppleAction,
    AppleDevice,
    Android,
    Mobile,
    Windows,
    AppleWindows,
    AndroidWindows,
    All,
    Unknown,
}

impl From<String> for Platform {
    fn from(value: String) -> Self {
        match value.as_str() {
            "apple" => Self::Apple,
            "apple-action" => Self::AppleAction,
            "apple-device" => Self::AppleDevice,
            "android" => Self::Android,
            "mobile" => Self::Mobile,
            "windows" => Self::Windows,
            "apple-windows" => Self::AppleWindows,
            "android-windows" => Self::AndroidWindows,
            "all" => Self::All,
            _ => Self::Unknown,
        }
    }
}
